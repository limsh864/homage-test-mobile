import axios from 'axios';
import { AsyncStorage } from 'react-native';

import API from '../controller/api';
import CONSTANTS from '../controller/constants';

import { FETCH_VISITS } from './types';

export const fetchVisits = (callback) => async dispatch => {
    try {
        const token = await AsyncStorage.getItem(CONSTANTS.TOKEN);
        var urls = [API.BASE_URL, API.VISIT_URL ];

        let { data } = await axios.get(urls.join('/'), {
            "headers": {
                'Authorization': token,
                'Content-Type': 'application/json'
            }
        });
        dispatch({ type: FETCH_VISITS, payload: data });
        callback();
    } catch (error) {
        
        console.log('@FETCH VISIT ERROR' - error);
        callback();
    }
}