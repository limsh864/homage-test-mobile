import _ from 'lodash';
import axios from 'axios';
import { AsyncStorage } from 'react-native';

import API from '../controller/api';
import CONSTANTS from '../controller/constants';
import {
    PROFILE_LOGIN,
    FETCH_PROFILE
} from './types';


export const updateLoginCredential = (loginCredential, callback) => async dispatch => {
    dispatch({ type: PROFILE_LOGIN, payload: loginCredential });
    callback();
}

export const fetchProfile = (callback) => async dispatch => {
    try {
        const token = await AsyncStorage.getItem(CONSTANTS.TOKEN)
        var urls = [API.BASE_URL, API.PROFILE];
        let { data } = await axios.get(urls.join('/'), {
            "headers": {
                'Authorization': token,
                'Content-Type': 'application/json'
            }
        });
        dispatch({ type: FETCH_PROFILE, payload: data });
        callback();
    } catch (error) {
        console.log(error);
    }
}

