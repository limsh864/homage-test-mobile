export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGOUT_SUCCESS = 'logout_success';
export const LOGOUT_FAIL = 'logout_fail';
export const GET_USER_ID_TOKEN = 'get_user_id_token';

export const FETCH_VISITS = 'fetch_visits';

export const PROFILE_LOGIN = 'profile_login';

export const FETCH_PROFILE = 'fetch_profile';