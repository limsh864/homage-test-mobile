import axios from 'axios';

import API from '../controller/api';
import { LOGIN_USER_SUCCESS } from './types';

export const loginUser = (email, password, callback) => async dispatch => {
    try {
        var urls = [API.BASE_URL, API.OWNER_LOGIN];
        let { data } = await axios.post(urls.join('/'),
            {
                "email": email,
                "password": password
            }
        );

        dispatch({ type: LOGIN_USER_SUCCESS, payload: data });
        callback();
    } catch (error) {
        console.log(error);
    }
}
