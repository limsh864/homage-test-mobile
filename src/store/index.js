import thunk from 'redux-thunk';
import { AsyncStorage } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistCombineReducers } from 'redux-persist';
import reducers from '../reducers';

const config = {
    key: 'root',
    storage: AsyncStorage
};

const reducer = persistCombineReducers(config, reducers);

export default function configurationStore(initialState = {}) {
    const store = createStore(
        reducer,
        initialState,
        applyMiddleware(thunk)
    );
    const persistor = persistStore(store);

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers/index');
            store.replaceReducer(nextRootReducer);
        });
    }

    return { persistor, store };
}
