import { Platform, StyleSheet, Dimensions, StatusBar } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import COLORS from './colors';

const window = Dimensions.get('window');

export const SCREEN_WIDTH = window.width;
export const SCREEN_HEIGHT = window.height;
export const IMAGE_HEIGHT = window.width / 3;

export default StyleSheet.create({
    container: {
        flex: 1,
        ...Platform.select({
            ios: {
                paddingTop: scale(30)
            },
            android: {
                paddingTop: verticalScale(4)
            }
        }),
    },
    centerContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    closeButton: {
        position: 'absolute',
        top: verticalScale(10),
        left: scale(10),
        color: 'white'
    },
    flaotingActionButton: {
        position: 'absolute',
        bottom: verticalScale(10),
        right: scale(10)
    },
    logoText: {
        fontSize: moderateScale(36),
        margin: scale(8),
        color: 'white',
        textAlign: 'center'
    },
    logoSubtitleText: {
        fontSize: moderateScale(18),
        color: 'white',
        textAlign: 'center',
        fontStyle: 'italic'
    },
    headerText: {
        fontSize: moderateScale(36),
        fontWeight: 'bold',
        color: COLORS.TEXT_HEADER,
        margin: scale(8)
    },
    titleText: {
        fontSize: moderateScale(30),
        fontWeight: 'bold',
        color: COLORS.TEXT_SECONDARY
    },
    subTitleText: {
        fontSize: moderateScale(20),
        color: COLORS.TEXT_PRIMARY
    },
    smallTitleText: {
        fontSize: moderateScale(16),
        color: COLORS.TEXT_PRIMARY
    },
    sectionListHeaderText: {
        backgroundColor: '#F0EFF5',
        fontSize: moderateScale(20),
        fontWeight: 'bold',
        color: COLORS.TEXT_HEADER,
        padding: scale(8)
    },
    textError: {
        color: 'red'
    },
    logo: {
        height: IMAGE_HEIGHT,
        resizeMode: 'contain',
        marginBottom: verticalScale(20),
        padding: scale(10),
        marginTop: verticalScale(20)
    },
    placeholderImageHeight: {
        height: verticalScale(200)
    },
    accentButtonStyle: {
        backgroundColor: COLORS.ACCENT_COLOR
    },
    roundedButtonStyle: {
        backgroundColor: COLORS.ACCENT_COLOR,
        borderRadius: scale(20)
    },
    roundedThemeTextButtonStyle: {
        backgroundColor: 'white',
        borderRadius: scale(20),
        borderColor: COLORS.THEME_COLOR
    },
    roundedTextInput: {
        height: verticalScale(50),
        backgroundColor: '#fff',
        marginHorizontal: scale(10),
        marginVertical: verticalScale(5),
        borderColor: COLORS.TEXT_HEADER,
        borderWidth: scale(1),
        borderRadius: scale(20),
        paddingVertical: verticalScale(5),
        paddingHorizontal: scale(15),
        width: window.width - scale(70)
    },
    textInput: {
        height: verticalScale(40),
        backgroundColor: '#fff',
        marginHorizontal: scale(10),
        marginVertical: verticalScale(5),
        borderColor: COLORS.TEXT_SECONDARY,
        borderWidth: scale(0.5),
        borderRadius: scale(8),
        paddingVertical: verticalScale(5),
        paddingHorizontal: scale(15),
        width: window.width - scale(30)
    },
    listPrimaryTextStyle: {
        fontSize: moderateScale(20),
        color: COLORS.TEXT_PRIMARY,
        margin: scale(4)
    },
    listSecondaryTextStyle: {
        fontSize: moderateScale(14),
        color: COLORS.TEXT_SECONDARY,
        margin: scale(4)
    },
    radioGroupStyle: {
        flex: 1,
        flexDirection: 'row',
        fontSize: 18
    },
    radioGroupContainer: {
        marginBottom: 0,
        marginTop: 0,
        backgroundColor: 'rgba(0, 0, 0, 0)',
        borderColor: 'rgba(0, 0, 0, 0)'
    },
    buttonGroupContainerStyle: {
        width: SCREEN_WIDTH,
        height: verticalScale(45),
        marginLeft: 0,
        marginRight: 0,
    },
    buttonGroupSelectedTextStyle: {
        color: COLORS.THEME_COLOR,
        fontWeight: 'bold'
    },
    filterButtonContainerStyle: {
        marginLeft: 0,
        marginRight: 0,
        marginBottom: verticalScale(2)
    },
    filterButtonStyle: {
        borderRadius: 2,
        borderWidth: 0.5,
        borderColor: COLORS.FOGGY,
        backgroundColor: 'white',
        height: verticalScale(45),
        marginLeft: scale(8),
        marginRight: scale(8),
        marginTop: verticalScale(4),
        marginBottom: verticalScale(4),
    },
    filterTextStyle: {
        width: "100%",
        textAlign: 'left',
        color: COLORS.TEXT_PRIMARY
    },
    leftRightContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginLeft: scale(8),
        marginRight: scale(8)
    },
    leftRightPadding: {
        paddingLeft: scale(8),
        paddingRight: scale(8)
    },
    footer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: verticalScale(10)
    },
    screenWidth: {
        width: SCREEN_WIDTH,
    }
});