import { Dimensions } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import COLORS from './colors';

const window = Dimensions.get('window');

export const SCREEN_WIDTH = window.width;

export default ({
    headerStyle: {
        fontSize: moderateScale(20),
        color: COLORS.TEXT_PRIMARY,
        margin: scale(8)
    },
    titleStyle: {
        fontSize: moderateScale(20),
        color: COLORS.TEXT_PRIMARY,
        marginTop: scale(8),
        marginLeft: scale(8),
        marginRight: scale(8)
    },
    indicatorStyles: {
        stepIndicatorSize: moderateScale(12),
        currentStepIndicatorSize: moderateScale(12),
        separatorStrokeWidth: moderateScale(12),
        currentStepStrokeWidth: 0,
        stepStrokeCurrentColor: COLORS.THEME_COLOR,
        stepStrokeWidth: 0,
        stepStrokeFinishedColor: COLORS.THEME_COLOR,
        stepStrokeUnFinishedColor: COLORS.FOGGY,
        separatorFinishedColor: COLORS.THEME_COLOR,
        separatorUnFinishedColor: COLORS.FOGGY,
        stepIndicatorFinishedColor: COLORS.THEME_COLOR,
        stepIndicatorUnFinishedColor: COLORS.FOGGY,
        stepIndicatorCurrentColor: COLORS.THEME_COLOR,
        stepIndicatorLabelFontSize: 0,
        currentStepIndicatorLabelFontSize: 0,
        stepIndicatorLabelCurrentColor: COLORS.THEME_COLOR,
        stepIndicatorLabelFinishedColor: COLORS.THEME_COLOR,
        stepIndicatorLabelUnFinishedColor: COLORS.FOGGY,
        labelColor: COLORS.TEXT_SECONDARY,
        labelSize: 0,
        currentStepLabelColor: COLORS.THEME_COLOR
    },
    listPrimaryTextStyle: {
        fontSize: moderateScale(20),
        color: COLORS.TEXT_PRIMARY,
        marginLeft: scale(4)
    },
    listSecondaryTextStyle: {
        fontSize: moderateScale(14),
        color: COLORS.TEXT_SECONDARY,
        marginLeft: scale(4)
    },
    interestedBOTextStyle: {
        fontSize: moderateScale(14),
        fontWeight: 'bold',
        color: COLORS.TEXT_PRIMARY
    },
    reviewTextStyle: {
        fontSize: moderateScale(18),
        fontWeight: 'bold',
        color: COLORS.TEXT_PRIMARY
    },
    acceptButtonStyle: {
        backgroundColor: COLORS.HAUS,
        width: (SCREEN_WIDTH / 2) - scale(45)
    },
    rejectButtonStyle: {
        backgroundColor: COLORS.RED,
        width: (SCREEN_WIDTH / 2) - scale(45)
    },
    textInput: {
        height: verticalScale(100),
        color: COLORS.TEXT_PRIMARY,
        backgroundColor: 'transparent',
        marginHorizontal: scale(8),
        marginVertical: verticalScale(8),
        borderColor: COLORS.FOGGY,
        borderWidth: scale(0.5),
        paddingVertical: verticalScale(8),
        paddingHorizontal: scale(8),
        textAlignVertical: "top"
    },
    addressTextInput: {
        color: COLORS.TEXT_PRIMARY,
        backgroundColor: 'transparent',
        marginHorizontal: scale(8),
        marginVertical: verticalScale(8),
        borderColor: COLORS.FOGGY,
        borderWidth: scale(0.5),
        paddingVertical: verticalScale(8),
        paddingHorizontal: scale(8),
        textAlignVertical: "top"
    },
    optionalTextStyle: {
        color: COLORS.TEXT_SECONDARY,
        margin: scale(4)
    }
});