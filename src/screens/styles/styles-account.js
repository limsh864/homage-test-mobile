import { StyleSheet } from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import COLORS from './colors';

export default StyleSheet.create({
    container: {
        backgroundColor: COLORS.THEME_COLOR,
        padding: scale(8)
    },
    headerText: {
        fontSize: moderateScale(36),
        color: 'white',
        alignSelf : 'center',
        marginBottom: verticalScale(16),
        marginLeft: scale(8)
    },
    textInput: {
        height: verticalScale(40),
        color: 'white',
        backgroundColor: 'transparent',
        marginHorizontal: scale(10),
        marginVertical: verticalScale(8),
        borderColor: 'white',
        borderWidth: scale(0.5),
        paddingVertical: verticalScale(5),
        paddingHorizontal: scale(15),
        width: window.width - scale(30)
    },
    profileTextInput: {
        height: verticalScale(40),
        color: COLORS.TEXT_PRIMARY,
        backgroundColor: 'transparent',
        borderColor: COLORS.TEXT_SECONDARY,
        borderWidth: scale(0.5),
        paddingVertical: verticalScale(4),
        paddingHorizontal: scale(4)
    },
    titleText: {
        fontSize: moderateScale(20),
        fontWeight: 'bold',
        color: COLORS.TEXT_SECONDARY,
        marginTop: verticalScale(8)
    },
    textError: {
        color: 'white'
    },
    sectionListHeaderText: {
        backgroundColor: COLORS.THEME_COLOR,
        fontSize: moderateScale(20),
        fontWeight: 'bold',
        color: 'white',
        padding: scale(8)
    },
});