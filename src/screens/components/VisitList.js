import _ from 'lodash';
import React, { Component } from 'react';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import COLORS from '../styles/colors';
import moment from 'moment'

class VisitList extends Component {
    static defaultProps = {
        onPress: () => { }
    }

    onRowPress() {
        this.props.onPress(this.props.item);
    }

    render() {
        const { item } = this.props;
        const { visitStartTime, visitEndTime } = item 
        var date = moment.unix(visitStartTime)
        var startTime = moment.unix(visitStartTime).format("hh:mm A");
        var endTime = moment.unix(visitEndTime).format("hh:mm A");
        return (
            <TouchableOpacity onPress={this.onRowPress.bind(this)}
                key={this.props.key}
                style={customStyles.container}
            >
                <View style={customStyles.columnContainer}>
                    <Image source={{ uri: item.careProfessional.picture }} style={{ width: scale(50), height: verticalScale(50), borderRadius : 25 }}/>
                <View style={customStyles.columnContainer}>
                    <View style={customStyles.contentViewStyle}>
                        <Text style={customStyles.titleTextStyle}>
                            {`Care Visit With ${item.careProfessional.fullname}`}
                        </Text>
                        <Text style={customStyles.textContainer}>
                            {`${date.format("dddd")} ${date.format("MM/DD/YYYY")} `}
                        </Text>
                        <Text style={customStyles.textContainer}>
                            {`${startTime} - ${endTime}`}
                        </Text>
                        <Text style={customStyles.textContainer}>
                            {`${item.location.address}`}
                        </Text>
                    </View>
                </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const customStyles = {
    container: {
        paddingLeft: scale(16),
        paddingRight: scale(16),
        paddingTop: verticalScale(16),
        paddingBottom: verticalScale(16),
        borderBottomWidth: verticalScale(0.5),
        borderColor: COLORS.TEXT_SECONDARY
    },
    columnContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    },
    contentViewStyle: {
        marginLeft: scale(8),
        width: 0,
        flexGrow: 1
    },
    titleTextStyle: {
        fontSize: moderateScale(16),
        fontWeight: 'bold',
        color: '#36cfd6',
        flexGrow: 1
    },
    textContainer: {
        marginTop: verticalScale(2),
        marginBottom: verticalScale(2)
    }
};

export default VisitList;