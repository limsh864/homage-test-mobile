import React, { Component } from 'react';
import { SafeAreaView, View, Text, FlatList, ActivityIndicator } from 'react-native';
import { Icon, ButtonGroup } from 'react-native-elements';
import { connect } from 'react-redux';
import axios from 'axios';
import { fetchVisits } from '../../../actions';
import API from '../../../controller/api';
import styles from '../../styles/styles';

import VisitList from '../../components/VisitList';


class CareProfessionalScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonIndex: 0,
            error: '',
            loading : true,
            visits : 0
        };
    }

    static navigationOptions = ({ navigation }) => {
        const { state } = navigation;
        return {
            header: null,
            tabBarIcon: ({ tintColor }) => (
                <Icon name="work" size={24} color={tintColor} />
            )
        };
    };

    async componentDidMount(){
        // var urls = [API.BASE_URL, API.VISIT_URL ];
        // let { data } = await axios.get(urls.join('/'), {
        //     "headers": { 'Content-Type': 'application/json'}
        // });
        //Added by Lim Sin Hong 25/2/2019 : Mock instead of getting from API
        data =  [
            {
                "location": {
                    "coordinates": [
                        3.1179,
                        101.6778
                    ],
                    "address": "Mid Valley"
                },
                "visitSummary": {
                    "bloodPressureSummary": [],
                    "heartRateSummary": []
                },
                "_id": "5c738a1504e25d4c8011b106",
                "careProfessional": { 
                    "id" : "VPMBOcF5bDY4yPwvPexYYNsz1gV2",
                    "email" : "limsh@gmail.com",
                    "fullname" : "Hong",
                    "mobile" : "0165441898",
                    "picture" : "https://i0.wp.com/heartland.cc/wp-content/uploads/2018/02/male-placeholder.jpg",
                    "location": {
                        "coordinates": [
                            3.1279,
                            101.6778
                        ],
                    },
                },
                "careRecipient": { 
                    "id" : "2",
                    "email" : "lim1@gmail.com",
                    "fullname" : "Recipient 2",
                    "mobile" : "0165441898",
                    "picture" : "https://i0.wp.com/heartland.cc/wp-content/uploads/2018/02/male-placeholder.jpg"
                },
                "careOwnerId": "r6sUJmsXuMgEGGWAe6mucyTBAOr2",
                "visitStartTime": 1551362400,
                "visitEndTime": 1551366000
            },
            {
                "location": {
                    "coordinates": [
                        3.1017,
                        101.6803
                    ],
                    "address": "Faber Heights"
                },
                "visitSummary": {
                    "bloodPressureSummary": [],
                    "heartRateSummary": []
                },
                "_id": "5c738a4c04e25d4c8011b107",
                "careProfessional": { 
                    "id" : "VPMBOcF5bDY4yPwvPexYYNsz1gV2",
                    "email" : "limsh@gmail.com",
                    "fullname" : "Hong",
                    "mobile" : "0165441898",
                    "picture" : "https://i0.wp.com/heartland.cc/wp-content/uploads/2018/02/male-placeholder.jpg",
                    "location": {
                        "coordinates": [
                            3.1279,
                            101.6778
                        ],
                    }
                },
                "careRecipient": { 
                    "id" : "1",
                    "email" : "lim@gmail.com",
                    "fullname" : "Recipient 1",
                    "mobile" : "0165441898",
                    "picture" : "https://i0.wp.com/heartland.cc/wp-content/uploads/2018/02/male-placeholder.jpg"
                },
                "careOwnerId": "r6sUJmsXuMgEGGWAe6mucyTBAOr2",
                "visitStartTime": 1551362400,
                "visitEndTime": 1551366000
            }
        ]
        if(data.length > 0){
            this.setState({ visits : data })
        }
        
    }

    availableVisits() {
        const { visits } = this.state;
        return (
            <FlatList
                data={visits}
                extraData={this.props}
                keyExtractor={item => item.job_id}
                onEndReached={this.state.isEnableFetchMore ? this.fetchMoreJobs : null}
                onEndReachedThreshold={0}
                renderItem={({ item }) => (
                    <VisitList
                        item={item}
                        onPress={item => this.onItemPressed(item)}
                    />
                )}
            />
        );
    }

    queryJobs(index) {
        switch (index) {
            case 0:
                return this.availableVisits();
            default:
                break;
        }
    }

    onItemPressed(item) {
        const { navigation } = this.props;
        navigation.navigate('visitDetails', {
            selectedItem: item
        });
    }

    renderLoading() {
        return (
            <ActivityIndicator size="large" />
        );
    }

    render() {
        const { buttonIndex, visits} = this.state;
        const buttons = ['Visits Happening Soon']
        return (
            <SafeAreaView style={[styles.container]}>
                <ButtonGroup
                    buttons={buttons}
                    selectedIndex={0}
                    onPress={this.updateButtonIndex}
                    containerStyle={styles.buttonGroupContainerStyle}
                    selectedTextStyle={styles.buttonGroupSelectedTextStyle}
                    containerBorderRadius={0} />
                {visits.length < 1 ? this.renderLoading() : this.queryJobs(buttonIndex)}
            </SafeAreaView>
        );
    }
}

const mapStateToProps = ({ visit }) => {
    const { visits } = visit;
    return { visits };
};

export default connect( mapStateToProps, { fetchVisits } )(CareProfessionalScreen);
