import _ from 'lodash';
import React, { Component } from 'react';
import { ScrollView, View, Text, Image, StyleSheet} from 'react-native';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'; 
import moment from 'moment'
import COLORS from '../../styles/colors';
import styles from '../../styles/styles';

class CareProfessionalScreen extends Component {
    constructor(props) {
        super(props);
    }

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarVisible: false
        };
    };

    renderInfo(selectedItem) {
        const { careProfessional, careRecipient, visitStartTime, visitEndTime} = selectedItem
        var date = moment.unix(visitStartTime)
        var startTime = moment.unix(visitStartTime).format("hh:mm A");
        var endTime = moment.unix(visitEndTime).format("hh:mm A");
        return (
            <View style={customStyles.viewMarginTop}>
                <Text style={[ customStyles.titleTextStyle, customStyles.firstLineHeight ]}>
                    {'Care Professional'}
                </Text>
                <View style={customStyles.columnContainer}>
                    <Image source={{ uri: careProfessional.picture }} style={{ width: scale(50), height: verticalScale(50), borderRadius : 25 }}/>
                    <View style={customStyles.viewMarginTop}>
                        <Text style={customStyles.headerText}>{'Name :'}</Text>
                        <Text style={{
                            marginTop: verticalScale(4),
                            marginBottom: verticalScale(10)
                        }}>
                            {careProfessional.fullname}
                        </Text>
                    </View>
                    <View style={customStyles.viewMarginTop}>
                        <Text style={customStyles.headerText}>{'Email :'}</Text>
                        <Text style={{
                            marginTop: verticalScale(4),
                            marginBottom: verticalScale(10)
                        }}>
                            {careProfessional.email}
                        </Text>
                    </View>
                    <View style={customStyles.viewMarginTop}>
                        <Text style={customStyles.headerText}>{'Mobile :'}</Text>
                        <Text style={{
                            marginTop: verticalScale(4),
                            marginBottom: verticalScale(10)
                        }}>
                            {careProfessional.mobile}
                        </Text>
                    </View>
                </View>
                <Text style={[ customStyles.titleTextStyle, customStyles.firstLineHeight ]}>
                    {'Care Recipient'}
                </Text>
                <View style={customStyles.columnContainer}>
                    <Image source={{ uri: careProfessional.picture }} style={{ width: scale(50), height: verticalScale(50), borderRadius : 25 }}/>
                    <View style={customStyles.viewMarginTop}>
                        <Text style={customStyles.headerText}>{'Name :'}</Text>
                        <Text style={{
                            marginTop: verticalScale(4),
                            marginBottom: verticalScale(10)
                        }}>
                            {careRecipient.fullname}
                        </Text>
                    </View>
                    <View style={customStyles.viewMarginTop}>
                        <Text style={customStyles.headerText}>{'Email :'}</Text>
                        <Text style={{
                            marginTop: verticalScale(4),
                            marginBottom: verticalScale(10)
                        }}>
                            {careRecipient.email}
                        </Text>
                    </View>
                    <View style={customStyles.viewMarginTop}>
                        <Text style={customStyles.headerText}>{'Mobile :'}</Text>
                        <Text style={{
                            marginTop: verticalScale(4),
                            marginBottom: verticalScale(10)
                        }}>
                            {careRecipient.mobile}
                        </Text>
                    </View>
                </View>
                <Text style={[ customStyles.titleTextStyle, customStyles.firstLineHeight ]}>
                    {'Visit Details'}
                </Text>
                <View style={customStyles.columnContainer}>
                    <View style={customStyles.viewMarginTop}>
                        <Text style={customStyles.headerText}>{'Date :'}</Text>
                        <Text style={{
                            marginTop: verticalScale(4),
                            marginBottom: verticalScale(10)
                        }}>
                            {`${date.format("dddd")} ${date.format("MM/DD/YYYY")} `}
                        </Text>
                    </View>
                    <View style={customStyles.viewMarginTop}>
                        <Text style={customStyles.headerText}>{'Time :'}</Text>
                        <Text style={{
                            marginTop: verticalScale(4),
                            marginBottom: verticalScale(10)
                        }}>
                            {`${startTime} - ${endTime}`}
                        </Text>
                    </View>
                    <View style={customStyles.viewMarginTop}>
                        <Text style={customStyles.headerText}>{'Address :'}</Text>
                        <Text style={{
                            marginTop: verticalScale(4),
                            marginBottom: verticalScale(10)
                        }}>
                            {selectedItem.location.address}
                        </Text>
                    </View>
                </View>
            </View>
        );
    }


    render() {
        const { state } = this.props.navigation;
        const { selectedItem } = state.params;
        return (
            <View style={{ flex: 1 }}>
                <ScrollView style={{ flex: 1 }}>
                <View style={customStyles.mapContainer}>
                    <MapView
                    provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                    style={customStyles.map}
                    region={{
                        latitude: selectedItem.location.coordinates[0],
                        longitude: selectedItem.location.coordinates[1],
                        latitudeDelta: 0.035,
                        longitudeDelta: 0.035,
                    }}>
                        <Marker  coordinate={{ latitude: selectedItem.location.coordinates[0], longitude: selectedItem.location.coordinates[1] }} pinColor={'red'} title={'Care Location'}></Marker>
                        <Marker  coordinate={{ latitude: selectedItem.careProfessional.location.coordinates[0], longitude: selectedItem.careProfessional.location.coordinates[1] }} pinColor={'green'} title={'Care Professional Location'}></Marker>
                    </MapView>
                </View>
                    <View style={[
                        styles.leftRightPadding, customStyles.container
                    ]}>
                        {this.renderInfo(selectedItem)}
                        
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const customStyles = {
    container: {
        marginTop: verticalScale(8),
        marginBottom: verticalScale(16)
    },
    viewMarginTop: {
        marginTop: verticalScale(8)
    },
    headerText: {
        color: '#36cfd6'
    },
    titleTextStyle: {
        fontSize: moderateScale(18),
        fontWeight: 'bold',
        color: COLORS.TEXT_PRIMARY
    },
    firstLineHeight: {
        height: verticalScale(28)
    },
    columnContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    },
    mapContainer: {
        height: 250,
        flex : 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
      },
    map: {
    ...StyleSheet.absoluteFillObject,
    }
}

export default CareProfessionalScreen;
