import React, { Component } from 'react';
import { View, Text, TextInput, AsyncStorage } from 'react-native';
import { Button } from 'react-native-elements';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { connect } from 'react-redux';

import { loginUser } from '../../actions';

import { isNullOrEmpty, isValidEmailInput } from '../../controller/StringUtils';

import CONSTANTS from '../../controller/constants';

import styles from '../styles/styles';
import accountStyles from '../styles/styles-account';

class LoginScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: null,
            password: null,
            showErrorText: false,
            errorMessage: '',
        };
    }

    async setProfilePreference() {
        const { token } = this.props;
        await AsyncStorage.setItem(CONSTANTS.TOKEN, token);
        console.log('@Navi - ', this.props)
        this.props.navigation.push('main');
    }

    async removeProfilePreference() {
        await AsyncStorage.removeItem(CONSTANTS.TOKEN);
    }

    async validLogin() {
        const { email, password } = this.state;

        this.props.loginUser(email, password, () => {
            const { auth } = this.props;
            console.log('@AUTH - ', this.props)
            if (auth.status) {
                this.setProfilePreference();
            } 
        });
    }

    render() {
        const { email, password } = this.state;
        return (
            < View style={[
                accountStyles.container,
                styles.container
            ]} >

                <Text style={accountStyles.headerText}>
                    {'Login'}
                </Text>

                <TextInput
                    placeholder={'Email'}
                    style={accountStyles.textInput}
                    keyboardType="email-address"
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                    placeholderTextColor='white'
                    autoCapitalize="none"
                    onChangeText={(text) => this.setState({ email: text })}
                />
                <TextInput
                    placeholder={'Password'}
                    style={accountStyles.textInput}
                    secureTextEntry
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                    placeholderTextColor='white'
                    autoCapitalize="none"
                    onChangeText={(text) => this.setState({ password: text })}
                />

                {
                    this.state.showErrorText &&
                    <Text style={[accountStyles.textError, { paddingHorizontal: scale(15) }]}>
                        {this.state.errorMessage}
                    </Text>
                }

                <Button
                    buttonStyle={[
                        styles.accentButtonStyle,
                        { marginTop: verticalScale(24) }
                    ]}
                    title={'Login'}
                    onPress={() => { 
                        this.props.loginUser(email, password, () => {
                            const { auth } = this.props;
                            console.log('@AUTH - ', this.props)
                            if (auth.status) {
                                this.setProfilePreference();
                            } 
                        });
                     }}
                />

            </View >
        );
    }
}

const mapStateToProps = ({ auth }) => {
    return { auth }
};

export default connect( mapStateToProps, { loginUser } )(LoginScreen);

