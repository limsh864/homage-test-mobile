export function isNullOrEmpty(text) {
    if (text === undefined || text === null) {
        return true;
    }

    const value = text.trim();
    return (value === undefined || value === "" || value.length === 0);
}

export function isValidURL(text) {
    if (isNullOrEmpty(text)) {
        return false;
    }

    var res = text.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if (res === null)
        return false;
    else
        return true;
}

export function isValidEmailInput(text) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
        console.log("Email is Not Correct");
        return false;
    }
    return true;
}

// Validates that the input string is a valid date formatted as "dd-mm-yyyy"
export function isValidDate(dateString) {
    // First check for the pattern
    if (!/^\d{1,2}\-\d{1,2}\-\d{4}$/.test(dateString))
        return false;

    // Parse the date parts to integers
    var parts = dateString.split("-");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if (year < 1000 || year > 3000 || month == 0 || month > 12)
        return false;

    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
};