import _ from 'lodash';
import axios from 'axios';
import { AsyncStorage } from 'react-native';

import API from '../controller/api';
import CONSTANTS from '../controller/constants';


export async function getProfile (){
    try {
        const token = await AsyncStorage.getItem(CONSTANTS.TOKEN)
        var urls = [API.BASE_URL, API.PROFILE];
        let { data } = await axios.get(urls.join('/'), {
            "headers": {
                'Authorization': token,
                'Content-Type': 'application/json'
            }
        });
        console.log(data)
        return data
    } catch (error) {
        console.log(error);
    }
}
