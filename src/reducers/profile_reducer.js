import _ from 'lodash';
import {
    PROFILE_LOGIN,
    FETCH_PROFILE,
} from '../actions/types';

export default (state = {}, action) => {
    switch (action.type) {
    
        case PROFILE_LOGIN:
            return {
                ...state,
                profileCredential: action.payload
            };
        case FETCH_PROFILE:
            return {
                ...state,
                profile: action.payload
            };
        default:
            return state;
    }
};