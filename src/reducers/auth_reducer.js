import _ from 'lodash';
import {
    LOGIN_USER_SUCCESS, LOGIN_USER_FAIL,
    LOGOUT_SUCCESS, LOGOUT_FAIL,
} from '../actions/types';

const INITIAL_STATE = {
    token: null,
    message: '',
    status: ''
};

export default (state = INITIAL_STATE, action) => {
    const token = _.get(action.payload, 'token');
    const message = _.get(action.payload, 'message');
    const status = _.get(action.payload, 'status');
    switch (action.type) {
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                ...INITIAL_STATE,
                token: token,
                message: message,
                status: status
            };
        case LOGIN_USER_FAIL:
            return {
                ...state,
                ...INITIAL_STATE,
                message: message,
                status: status
            };
        case LOGOUT_SUCCESS:
            return { ...state, ...INITIAL_STATE };
        case LOGOUT_FAIL:
            return { ...state, message: message, status: status };
        default:
            return state;
    }
};