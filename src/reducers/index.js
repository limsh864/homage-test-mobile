import auth from './auth_reducer';
import visit from './visit_reducer';
import profile from './profile_reducer';

export default ({
    auth, visit, profile
});
