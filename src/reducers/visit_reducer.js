import _ from 'lodash';
import {
    FETCH_VISITS
} from '../actions/types';

const INITIAL_STATE = {
    visits: [],
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case FETCH_VISITS:
        console.log('@ACTION - ', action)
            if (action.payload.length > 0) {
                return { ...state, visits : action.payload};
            }
        default:
            return state;
    }
};