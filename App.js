import React, { Component } from 'react';
import { Platform, Text, View, AsyncStorage } from 'react-native';
import { StackNavigator, TabNavigator, TabBarBottom } from 'react-navigation';
import { Provider, connect } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
import { YellowBox } from 'react-native';

import configureStore from './src/store';
import CONSTANTS from './src/controller/constants';

import COLORS from './src/screens/styles/colors';

import LoginScreen from './src/screens/account/LoginScreen';

import CareProfessionalScreen from './src/screens/content/home/CareProfessionalScreen';
import CareProfessionalDetails from './src/screens/content/home/CareProfessionalDetails';

class Container extends Component {
  constructor() {
    super();
    this.state = {
      signedIn: null
    }
  }

  async componentDidMount() {
    YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);
    YellowBox.ignoreWarnings(["Warning: Can't call setState (or forceUpdate)", "Module RCTImageLoader"]);

    this.init()
  }

  async init() {
    const token = await AsyncStorage.getItem(CONSTANTS.TOKEN)
    if (token !== null) {
      this.setState({ signedIn: true });
    }
  }

  componentWillUnmount() {
    // this.messageListener();
  }

  render() {

    const AccountNavigator = StackNavigator({
      login: { screen: LoginScreen }
    }, {
        navigationOptions: ({ navigation, screenProps }) => ({
          headerStyle: {
            backgroundColor: COLORS.THEME_COLOR,
            borderWidth: 0, borderBottomColor: COLORS.THEME_COLOR
          },
          headerTitleStyle: { color: 'white' },
          headerBackTitleStyle: 'white',
          headerTintColor: 'white'
        })
      });

    const VisitNavigator = StackNavigator({
      allVisits: { screen: CareProfessionalScreen },
      visitDetails : { screen : CareProfessionalDetails }
    });

    const HomeNavigator = TabNavigator({
      visit: { screen: VisitNavigator, navigationOptions: { title:'Pending Visits' } }
    }, {
        tabBarOptions: {
          showLabel: true,
          showIcon: true,
          activeTintColor: COLORS.THEME_COLOR,
          inactiveTintColor: COLORS.TEXT_SECONDARY
        },
        tabBarComponent: TabBarBottom,
        tabBarPosition: 'bottom',
        animationEnabled: false,
        swipeEnabled: false
      });

    const MainNavigator = StackNavigator({
      account: { screen: AccountNavigator },
      main: { screen: HomeNavigator }
    }, {
        headerMode: 'none',
        mode: 'modal',
        navigationOptions: {
          gesturesEnabled: false
        },
        backBehavior: 'none',
        initialRouteName: 'main'
      });

    return (
        <View style={{ flex: 1 }}>
          <MainNavigator />
        </View>
    );
  }
}

const { persistor, store } = configureStore();
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <AppContainer />
        </PersistGate>
      </Provider>
    );
  }
}

// AppContainer class
const mapStateToProps = (state) => {
  return state
};

const AppContainer = connect(mapStateToProps)(Container);